package ru.pyshinskiy.tm.api.project;

import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveRepository;
import ru.pyshinskiy.tm.entity.Project;

public interface IProjectRepository extends IAbstractObjectiveRepository<Project> {
}
