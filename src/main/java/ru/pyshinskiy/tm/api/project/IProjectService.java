package ru.pyshinskiy.tm.api.project;

import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveService;
import ru.pyshinskiy.tm.entity.Project;
public interface IProjectService extends IAbstractObjectiveService<Project> {

}
