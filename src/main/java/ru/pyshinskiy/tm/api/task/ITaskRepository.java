package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveRepository;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractObjectiveRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;
}
