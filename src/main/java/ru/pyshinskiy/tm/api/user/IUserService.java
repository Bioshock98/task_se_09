package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserService {

    @Nullable
    User findOne(@Nullable final String id) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User persist(@Nullable final User user) throws Exception;

    @Nullable
    User merge(@Nullable final User user) throws Exception;

    @Nullable
    User remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @Nullable
    String getIdByNumber(final int number) throws Exception;
}
