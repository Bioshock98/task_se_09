package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(@NotNull final String id) throws Exception;

    @Nullable
    User persist(@NotNull final User user) throws Exception;

    @Nullable
    User merge(@NotNull final User user) throws Exception;

    @Nullable
    User remove(@NotNull final String id) throws Exception;

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    void removeAll() throws Exception;
}
