package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public final class ProjectService extends AbstractObjectiveService<Project> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @NotNull
    public List<Project> sortByCreateTime(@NotNull final List<Project> objectives, final int direction) throws Exception {
        return objectiveRepository.sortByCreateTime(objectives, direction);
    }

    @NotNull
    @Override
    public List<Project> sortByStartDate(@NotNull final List<Project> objectives, final int direction) throws Exception {
        return objectiveRepository.sortByStartDate(objectives, direction);
    }

    @Override
    @NotNull
    public List<Project> sortByEndDate(@NotNull final List<Project> objectives, final int direction)throws Exception  {
        return objectiveRepository.sortByEndDate(objectives, direction);
    }

    @Override
    @NotNull
    public List<Project> sortByStatus(@NotNull final List<Project> objectives, final int direction) throws Exception {
        return objectiveRepository.sortByStatus(objectives, direction);
    }
}
