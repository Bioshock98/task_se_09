package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveRepository;
import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveService;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractObjectiveService<T extends AbstractEntity> extends AbstractService<T> implements IAbstractObjectiveService<T> {

    @NotNull protected final IAbstractObjectiveRepository<T> objectiveRepository = (IAbstractObjectiveRepository<T>) abstractRepository;

    public AbstractObjectiveService(@NotNull final IAbstractObjectiveRepository<T> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return objectiveRepository.findOne(userId, id);
    }

    @Override
    @NotNull
    public List<T> findAll(@Nullable final String userId) throws Exception{
        if(userId == null) throw new Exception("userId is null");
        return objectiveRepository.findAll(userId);
    }

    @Override
    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return objectiveRepository.remove(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        objectiveRepository.remove(userId);
    }

    @Override
    public @Nullable List<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || name == null) throw new Exception("one of the parameters passed is null");
        return objectiveRepository.findByName(userId, name);
    }

    @Override
    public @Nullable List<T> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || description == null) throw new Exception("one of the parameters passed is null");
        return objectiveRepository.findByDescription(userId, description);
    }
}
