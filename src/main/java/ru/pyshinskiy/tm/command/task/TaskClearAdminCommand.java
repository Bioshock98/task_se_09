package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

public final class TaskClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "task_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "clear all users tasks";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getTaskService().removeAll();
    }
}
