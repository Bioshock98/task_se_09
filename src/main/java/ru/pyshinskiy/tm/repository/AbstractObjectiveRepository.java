package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.objective.IAbstractObjectiveRepository;
import ru.pyshinskiy.tm.entity.AbstractObjective;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractObjectiveRepository<T extends AbstractObjective> extends AbstractRepository<T> implements IAbstractObjectiveRepository<T> {

    @NotNull
    public List<T> findAll(@NotNull final String userId) throws Exception {
        @NotNull final LinkedList<T> objectives = new LinkedList<>();
        for(@NotNull final T objective : findAll()) {
            assert objective.getUserId() != null;
            if(objective.getUserId().equals(userId)) objectives.add(objective);
        }
        return objectives;
    }

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        for(@NotNull final T objective : findAll()) {
            assert objective.getId() != null;
            if(objective.getId().equals(id)) return objective;
        }
        return null;
    }

    @Nullable
    public T remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityMap.remove(id);
    }

    public void removeAll(@NotNull final String userId) throws Exception {
        for(@NotNull final T objective : findAll()) {
            assert objective.getUserId() != null;
            if(objective.getUserId().equals(userId)) {
                assert objective.getId() != null;
                remove(objective.getId());
            }
        }
    }

    @Nullable
    @Override
    public List<T> findByName(@NotNull String userId, @NotNull String name) throws Exception {
        @NotNull final List<T> objectives = new LinkedList<>();
        for(@NotNull final T t : findAll(userId)) {
            assert t.getName() != null;
            if(name.equals(t.getName()) || t.getName().contains(name)) objectives.add(t);
        }
        return objectives;
    }

    @Nullable
    @Override
    public List<T> findByDescription(@NotNull String userId, @NotNull String description) throws Exception {
        @NotNull final List<T> objectives = new LinkedList<>();
        for(@NotNull final T t : findAll(userId)) {
            assert t.getDescription() != null;
            if(description.equals(t.getDescription()) || t.getDescription().contains(description)) objectives.add(t);
        }
        return objectives;
    }
}
