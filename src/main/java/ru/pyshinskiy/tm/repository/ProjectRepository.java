package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractObjectiveRepository<Project> implements IProjectRepository {

    @Override
    public @NotNull List<Project> sortByCreateTime(@NotNull final List<Project> projects, final int direction) throws Exception {
        projects.sort(new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getCreateDate().compareTo(o2.getCreateDate());
            }
        });
        return projects;
    }

    @Override
    public @NotNull List<Project> sortByStartDate(@NotNull final List<Project> projects, final int direction) throws Exception {
        projects.sort(new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                assert o1.getStartDate() != null;
                assert o2.getStartDate() != null;
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });
        return projects;
    }

    @Override
    public @NotNull List<Project> sortByEndDate(@NotNull final List<Project> projects, final int direction) throws Exception {
        projects.sort(new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                assert o1.getEndDate() != null;
                assert o2.getEndDate() != null;
                return o1.getEndDate().compareTo(o2.getEndDate());
            }
        });
        return projects;
    }

    @Override
    public @NotNull List<Project> sortByStatus(@NotNull final List<Project> projects, final int direction) throws Exception {
        projects.sort(new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                assert o1.getStatus() != null;
                assert o2.getStatus() != null;
                if (o1.getStatus().equals(o2.getStatus())) return 0;
                else if (o1.getStatus().equals(Status.PLANNED)) return -1;
                else if (o2.getStatus().equals(Status.PLANNED)) return 1;
                else if (o1.getStatus().equals(Status.IN_PROGRESS)) return -1;
                else if (o2.getStatus().equals(Status.IN_PROGRESS)) return 1;
                return 1;
            }
        });
        return projects;
    }
}
