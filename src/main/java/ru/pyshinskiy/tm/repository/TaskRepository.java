package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractObjectiveRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final LinkedList<Task> projectTasks = new LinkedList<>();
        for(@NotNull final Task task : findAll(userId)) {
            assert task.getProjectId() != null;
            if(task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        for(@NotNull final Task task : findAll(userId)) {
            if(projectId.equals(task.getProjectId())) {
                assert task.getId() != null;
                remove(userId, task.getId());
            }
        }
    }

    @Override
    public @NotNull List<Task> sortByCreateTime(@NotNull final List<Task> tasks, final int direction) throws Exception {
        tasks.sort(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getCreateDate().compareTo(o2.getCreateDate());
            }
        });
        return tasks;
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@NotNull final List<Task> tasks, final int direction) throws Exception {
        tasks.sort(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                assert o1.getStartDate() != null;
                assert o2.getStartDate() != null;
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });
        return tasks;
    }

    @Override
    public @NotNull List<Task> sortByEndDate(@NotNull final List<Task> tasks, final int direction) throws Exception {
        tasks.sort(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                assert o1.getEndDate() != null;
                assert o2.getEndDate() != null;
                return o1.getEndDate().compareTo(o2.getEndDate());
            }
        });
        return tasks;
    }

    @Override
    public @NotNull List<Task> sortByStatus(@NotNull final List<Task> tasks, final int direction) throws Exception {
        tasks.sort(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                assert o1.getStatus() != null;
                assert o2.getStatus() != null;
                if (o1.getStatus().equals(o2.getStatus())) return 0;
                else if (o1.getStatus().equals(Status.PLANNED)) return -1;
                else if (o2.getStatus().equals(Status.PLANNED)) return 1;
                else if (o1.getStatus().equals(Status.IN_PROGRESS)) return -1;
                else if (o2.getStatus().equals(Status.IN_PROGRESS)) return 1;
                return 1;
            }
        });
        return tasks;
    }
}
